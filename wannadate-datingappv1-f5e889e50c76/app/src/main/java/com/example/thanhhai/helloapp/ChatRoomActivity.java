package com.example.thanhhai.helloapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class ChatRoomActivity extends AppCompatActivity {

    DatabaseReference myRef;
    String id;

    ImageButton btnAccept,btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        myRef =  FirebaseDatabase.getInstance().getReference();
        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        getId();
        event();
    }



    void event(){
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent =new Intent(ChatRoomActivity.this,MatchingActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);*/
                //finish();
                Toast.makeText(ChatRoomActivity.this, "Đồng ý hẹn hò",
                        Toast.LENGTH_LONG).show();

            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent =new Intent(ChatRoomActivity.this,UserInfomationActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
                finish();
            }
        });

    }

    void getId(){

        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnAccept = (ImageButton) findViewById(R.id.btnAgree);

    }


}

package com.example.thanhhai.helloapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class RoomListMCActivity extends AppCompatActivity {
    private ListView lvRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_list_mc);

        lvRoom=findViewById(R.id.lvSchedule);

        ArrayList<Room> arrayList=new ArrayList<>();
        Room model1=new Room("1000","10:00AM 1/1/2019");
        Room model2=new Room("1001","11:00AM 1/1/2019");
        Room model3=new Room("1002","01:00PM 1/1/2019");
        Room model4=new Room("1003","10:00AM 3/1/2019");
        Room model5=new Room("1004","10:00AM 2/1/2019");
        Room model6=new Room("1005","10:00AM 4/1/2019");
        Room model7=new Room("1006","10:00AM 5/1/2019");
        Room model8=new Room("1007","10:00AM 6/1/2019");
        arrayList.add(model1);
        arrayList.add(model2);
        arrayList.add(model3);
        arrayList.add(model4);
        arrayList.add(model5);
        arrayList.add(model6);
        arrayList.add(model7);
        arrayList.add(model8);

        CustomAdapter customAdapter=new CustomAdapter(this,R.layout.target_item,arrayList);
        lvRoom.setAdapter(customAdapter);
    }
}

package com.example.thanhhai.helloapp;

import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class WelcomeActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT=2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                autoLogin();
            }
        },SPLASH_TIME_OUT);
    }

    void autoLogin() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference();
        Query query = myRef.child("SaveLogin").orderByChild("idPhone").equalTo(getAndroidId()).limitToFirst(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        SaveLogin saveLogin = data.getValue(SaveLogin.class);
                        if(saveLogin.isOnLogin){
                            Intent intent = new Intent(WelcomeActivity.this,UserInfomationActivity.class);
                            intent.putExtra("id",saveLogin.idUser);
                            startActivity(intent);
                            finish();

                        }
                        else if(!saveLogin.isOnLogin){
                            Intent intent = new Intent(WelcomeActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public String getAndroidId(){
        String androidId =Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return  androidId;
    }
}
